# Symfony Login form
A basic walktough to create a Symfony project with secure login.
Based of Symfony officail walktrough.<br>
Environment: Local, XAMPP (3.2.3), MariaDB (10.1.38 -> with upgrade to 10.3.15)

### Part 1: entity and db
* Create Symfony project named `` login -form``<br>
  `` composer create-project symfony/website-skeleton login-form `` 
* Create entity ``User`` (keep defaults, this one uses ``argon2i``): <br>
  `` php bin/console make:user ``
* Edit `` .env`` file:<br>
  ``DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/test_symfony_login``
* Create db: <br>
  `` php bin/console doctrine:database:create`` 
* Make migration<br>
  ``php bin/console make:migration``

### Part 2: login with entity
* Create authenticator <br>
  ``php bin/console make:auth`` -> ``1`` -> ``LoginFormAuthenticator`` -> ``SecurityController`` 
* See your login at [127.0.0.1:8000/login](http://127.0.0.1:8000/login)<br>
  ``symfony serve``

### Part 3: registration form
* With terminal<br>
  ``make:registration-form `` -> @UniqueEntity? -> ``yes``<br>
  -> automatically authenticate the user after registration? -> ``yes``
* View register form at  [127.0.0.1:8000/register](http://127.0.0.1:8000/register)
* Create database for ``User``<br>
  * See changes: ``php bin\console doctrine:schema:update --dump-sql``
  * Edit entity User (User entity's table name is "app_users" because "USER" is a SQL reserved word.)<br>
  ``@ORM\Table(name="app_users")`` 
    ```
    /**
    * @ORM\Table(name="app_users")
    * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
    * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
    */ 
    ```
  * ``php bin/console doctrine:schema:update --force``
* Provide a valid redirect inside LoginFormAuthenticator.php (at TODO)
  ``return new RedirectResponse($this->urlGenerator->generate('app_login'));``
* Test [register](http://127.0.0.1:8000/register) and [login](http://127.0.0.1:8000/login) or have a look at the database: <br>
  `` php bin/console doctrine:query:sql "select * from app_users" ``


### Part 4: logout
* Create logout route
   ```
    // src/Controller/SecurityController.php
    /**
      * @Route("/logout", name="app_logout", methods={"GET"})
      */
    public function logout()
    {
        // controller can be blank: it will never be executed!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
    // Security.yaml
    security:
      firewalls:
        main
          logout:
            path: app_logout
    ```

### Part 5: create admin role: 
* check current user(s) in database:<br>
  ``php bin\console doctrine:query:sql "select * from app_users"`` 
* set new user role for user:<br>
  ``UPDATE `test_symfony_login`.`app_users` SET `roles`='["ROLE_ADMIN"]' WHERE  `id`=1;`` 
* login with user where id = 1, create ``{{ dump() }}`` inside base.html.twig; and have a look what user 1 looks like on base.html.twig<br>
  `` array:3 -> "app" -> tokenStorage -> token -> roles[0] (array:2) -> role: "ROLE_ADMIN"`` <br>
  and/or 
  ```  
  {# base.html.twig #}
  {{ dump() }}
  {% if is_granted('IS_AUTHENTICATED_FULLY') %}
      <p>Roles:
          <ul>
              {% for role in app.user.roles %}
                  <li>{{ role }}</li>
              {% endfor %}
          </ul>
      </p>
  {% endif %}
  ``` 
* create some basic auth testing
  ``` 
    /**
     * @Route("/admin", name="app_admin")
     */
    public function adminPath()
    {
        // GOOD - use of the normal security methods
        $hasAccess = $this->isGranted('ROLE_ADMIN');
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        dd("access role is at least ROLE_ADMIN", $hasAccess);
    }
    /**
     * @Route("/user", name="app_user")
     */
    public function userPath()
    {
        // GOOD - use of the normal security methods
        $hasAccess = $this->isGranted('ROLE_USER');
        $this->denyAccessUnlessGranted('ROLE_USER');

        dd("access role is at least ROLE_USER", $hasAccess);
    }
    ``` 
    -> test by going to [/user](http://127.0.0.1:8000/user) and [/admin](http://127.0.0.1:8000/admin)


----

#### Possible errors and fixes:
 ``` 
 Schema-Tool failed with Error 'An exception occurred while executing 'CREATE TABLE app_users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NU   
  LL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_C2502824E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB':
```
-> MariaDB can't use JSON, you should change ``JSON NOT NULL`` to ``LONGTEXT NOT NULL``, or upgrade to version 10.2.7 and newer.<br>I did an upgrade from 10.1 to 10.2, and then to 10.3. (on windows this is possible with the msi files)

----


#### Sources:
* [symfony.com/doc/current/security/form_login_setup](https://symfony.com/doc/current/security/form_login_setup)
* [symfony.com/doc/current/doctrine.html](https://symfony.com/doc/current/doctrine.htm)
* [symfony.com/doc/4.0/security/entity_provider.html](https://symfony.com/doc/4.0/security/entity_provider.html )
* [symfony.com/doc/current/doctrine/registration_form.html](https://symfony.com/doc/current/doctrine/registration_form.html)
* [mariadb.com/kb/en/library/json-data-type/](https://mariadb.com/kb/en/library/json-data-type/)
* [stackoverflow.com/questions/44027926/update-xampp-from-maria-db-10-1-to-10-2 ](https://stackoverflow.com/a/48848806/4249483)